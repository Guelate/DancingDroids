// Seyo Guelate (seul).
//18904103 .




use std::io;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;




#[derive(Debug , PartialEq)]
enum Orientation {
    North,
    West,
    Est,
    South,
}

#[derive(Debug, PartialEq)]
enum Instruction{
    L,
    R,
    F,
}

#[derive(Debug,PartialEq)]

struct Robot{
    id: i32,
    x: i32,
    y: i32,
    orientation: Orientation, 
    instruction: Vec<Instruction>,
}


// fonction qui permet de choisir une orientation par la lecture d'un char .

fn choix_o(name: char) -> Orientation{

    
    match &name {
        'N' =>Orientation::North,
        'W' =>Orientation::West,
        'E' =>Orientation::Est,
        'S' =>Orientation::South,
        _=>Orientation::North, //Orientation par défaut .
    }

}


// fonction qui permet de choisir une instruction par la lecteur d'un char.

fn choix_i(name: char) -> Instruction{

    
    match &name {
        'L' =>Instruction::L,
        'R' =>Instruction::R,
        'F' =>Instruction::F,
        _=>Instruction::R, //Instruction par défaut .

    }
}

// fonction qui va permettre le déplacement d'un robot .

fn load(r: &mut Robot , tab :&mut Vec<Instruction>){
    for v in tab{
         match v{
             
            Instruction::F => match r.orientation{ 

                Orientation::North => r.y += 1,
                Orientation::West => r.x -= 1,
                Orientation::Est => r.x += 1,
                Orientation::South => r.y -= 1,
            }
            
            Instruction::R => match r.orientation{

                 Orientation::North => r.orientation = Orientation::Est,
                 Orientation::Est => r.orientation = Orientation::South,
                 Orientation::South => r.orientation = Orientation::West,
                 Orientation::West => r.orientation = Orientation::North,
                 
            }

            Instruction::L => match r.orientation{
                
                Orientation::North => r.orientation = Orientation::West,
                Orientation::West => r.orientation = Orientation::South,
                Orientation::South => r.orientation = Orientation::Est,
                Orientation::Est => r.orientation = Orientation::North,

            }

        }

    }

}


// fonction coallision 

fn coallision(r: &mut Robot , r1: &mut Robot)-> bool{ 

        if r.y == r1.y || r.x == r1.y{
            true
        }else{false}


    }


// test des fonction choix d'orientation + d'instruction.

#[cfg(test)]

mod test{
    use super::*; 
    #[test]

    fn premier(){
        assert_eq!(choix_o('N'),Orientation::North); //La fonction choix pour l'orientation est fonctionnel .
    }

    #[test]

    fn deuxieme(){
        assert_eq!(choix_i('F'),Instruction::F);//La fonction choix pour l'instruction est fonctionnel.
    }

}



fn main(){ 

let mut robots: Vec<Robot> = Vec::new();

// premier robot .

   let mut robot1 = Robot{

        id : 1,
        x : 1,
        y : 1,
        orientation : Orientation::North,
        instruction : vec![],

    };


// deuxieme robot.
    let mut robot2 = Robot{

        id : 2,
        x : 0,
        y : 1,
        orientation : Orientation::West,
        instruction : vec![],

    };


println!("La position de départ du robot {} : est x:{} y:{} avec l'orientation: {:?}",robot1.id,robot1.x,robot1.y,robot1.orientation);
let mut vec1: Vec<Instruction> = Vec::new();

// lecture du fichier d'instruction pour attribuer les instruction au robot .

let mut fichier = File::open("robot1.txt").expect("can't open file");
let reader = BufReader::new(fichier);


for line in reader.lines(){

     for v in line.expect("line illisible").chars(){

         match v{


            'L' => vec1.push(choix_i('L')),
            'R' => vec1.push(choix_i('R')),
            'F' => vec1.push(choix_i('F')),
             _=> load(&mut robot1,&mut vec1),
        }
      

    }

    
}


coallision(&mut robot1,& mut robot2);


println!("La position final du robot {} : est x:{} y:{} avec l'orientation: {:?}",robot1.id,robot1.x,robot1.y,robot1.orientation);


}

/*  Dans ce bout de code qui concerce uniquement la première version . le but étaitde pouvoir changer 
le positionnement des robots par la fonction load(). Cela par la lecture du fichier d'instruction.
J'ai été bloqué au moment de faire le même mécanisme pour le deuxième robot en lisant le fichier
j'ai eu du mal à extraire les instructions du deuxième robot. Du coup j'ai seulement afficher l'état 
final du premier robot afin de vous montrer l'idée . */